<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<?
### RECURSIVO #####
 if (!$_POST)   { #           
###################
?>
<?
require('conectabd.php');

$IDmodelo =  $_GET['IDmodelo'];
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDmodelo'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
$Arquivo = explode('.', $dados->PortNomeArquivo);
if (isset($dados->PortIDVersaoAnterior)){$IDmodeloAnterior = $dados->PortIDVersaoAnterior;}else{$IDmodeloAnterior = NULL;}
?>
<font face=arial>
<title>Editar Modelo</title>
<h4 align=center>Editar Modelo de Processo no Portfólio de Processos AGU</h4>
<form method=post enctype="multipart/form-data" action="<? $PHP_SELF ?>"> 
<table align=center>
<tr><td>ID:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=IDmodelo value="<?=$dados->PortID?>" title='ID do modelo' readonly></td></tr>
<tr><td>Versão:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=Versao value="<?=$dados->PortVersao?>" title='Versão do modelo'></td></tr>
<tr><td><i>ID Versão anterior</i>:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=IDmodeloAnterior value="<?=$IDmodeloAnterior?>" title="ID do modelo da versão anterior" readonly></td></tr>
<tr><td>Nome Processo:</td><td><input type=text size=40 name=NomeProcesso value="<?=$dados->PortNomeProcesso?>" required></td></tr>
<tr><td>CPF Gestor:</td><td><input type=text size=40 name=CPFGestor value="<?=$dados->PortCPFgestor?>" required></td></tr>
<tr><td>Email Gestor:</td><td><input type=email size=40 name=EmailGestor value="<?=$dados->PortEmailGestor?>" required></td></tr>
<tr><td>Lotação Gestor:</td><td><input type=text size=40 name=LotacaoGestor value="<?=$dados->PortLotacaoGestor?>" required></td></tr>
<tr><td>Finalidade:</td><td><select name=Finalidade required>
<option value=''>Selecione a finalidade do processo</option>
<option selected><?=$dados->PortFinalidade?></option>
<option>Finalístico</option>
<option>Governança</option>
<option>Controle</option>
<option>Gestão</option>
</select></td></tr>
<tr><td>Abrangência:</td><td><select name=Abrangencia required>
<option value=''>Selecione a abrangência do processo</option>
<option selected><?=$dados->PortAbrangencia?></option>
<option>Corporativo</option>
<option>Setorial</option>
</select></td></tr>
<tr><td>Cadeia de Valor:</td><td><select name=CadeiaDeValor required>
<option value=''>Selecione a posição do processo na Cadeia de Valor</option>
<option selected><?=$dados->PortCadeiaDeValor?></option>
<option>Definir Diretrizes e Estratégia</option>
<option>Manter Relacionamentos</option>
<option>Entregar Produtos e Serviços</option>
<option>Suportar Processos</option>
</select></td></tr>
<tr><td>Hierarquia:</td><td><select name=Hierarquia required>
<option value=''>Selecione a posição hierarquica do processo</option>
<option selected><?=$dados->PortHierarquia?></option>
<option>Macroprocesso</option>
<option>Processo</option>
<option>Subprocesso</option>
</select></td></tr>
<tr><td>Detalhamento:</td><td><select name=Detalhamento required>
<option value=''>Selecione o nível de detalhamento do processo</option>
<option selected><?=$dados->PortDetalhamento?></option>
<option>Visão Geral</option>
<option>Negócio</option>
<option>Sistema</option>
</select></td></tr>
<tr><td>Indicadores:</td><td><input type=text size=40 name=Indicadores value="<?=$dados->PortIndicadores?>" required></td></tr>
<tr><td>Maturidade:</td><td><select name=Maturidade required>
<option value=''>Selecione a maturidade do processo</option>
<option selected><?=$dados->PortMaturidade?></option>
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
<option>6</option>
</select></td></tr>
<tr><td>Situação:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=Situacao value="<?=$dados->PortSituacao?>" required readonly></td></tr>
<tr><td>Nome Arquivo:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=NomeArquivo value="<?=$dados->PortNomeArquivo?>" required readonly></td></tr>
<tr><td>Data modificação:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=DataModificacao value="<?=$dados->PortDataModificacao?>" required readonly></td></tr>
<tr><td>Ip que modificou:</td><td><input type=text style='background-color:#BDBDBD' size=40 name=IPmodicficou value="<?=$dados->PortIPModificacao?>" required readonly></td></tr>
<tr><td>Substituir documentação:</td><td><input type="file" name="arquivo"></td>
<tr><td>Substituir diagrama:</td><td><input type="file" name="diagrama"></td>
<input type="hidden" name="MAX_FILE_SIZE" value="1024">
</tr>
<tr><td colspan=2 align=center><br><input type="submit" name="Atualizar" value="Atualizar"></td></tr>
<input type="hidden" name=DataAtual value="<?=date('Y-m-d H:i:s')?>">
<input type="hidden" name=IPusuario value="<?=$_SERVER['REMOTE_ADDR']?>">
</table>
</form> 
<br><br>
<p align='center'><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>
<?
 }
### RECURSIVO ###
  } else {      #           
#################

$IDmodelo      = $_POST['IDmodelo'];
$Versao        = $_POST['Versao'];
if (isset($_POST['IDmodeloAnterior'])){$IDmodeloAnterior = $_POST['IDmodeloAnterior'];}else{$IDmodeloAnterior = NULL;}
$NomeProcesso  = $_POST['NomeProcesso'];
$CPFGestor     = $_POST['CPFGestor'];
$EmailGestor   = $_POST['EmailGestor'];
$LotacaoGestor = $_POST['LotacaoGestor'];
$Finalidade    = $_POST['Finalidade'];
$Abrangencia   = $_POST['Abrangencia'];
$CadeiaDeValor = $_POST['CadeiaDeValor'];
$Hierarquia    = $_POST['Hierarquia'];
$Detalhamento  = $_POST['Detalhamento'];
$Indicadores   = $_POST['Indicadores'];
$Maturidade    = $_POST['Maturidade'];
$DataAtual     = $_POST['DataAtual'];
$IPusuario     = $_POST['IPusuario'];
$NomeArquivo   = $_POST['NomeArquivo']; 
$NovoArquivo   = $_FILES['arquivo']['name'];
$NovoDiagrama  = $_FILES['diagrama']['name'];

require('conectabd.php');

//Atualiza metadados do modelo de processo
$atualizar = $conecta->exec("UPDATE TabPortfolio SET PortNomeProcesso='$NomeProcesso', PortCPFgestor='$CPFGestor', PortEmailGestor='$EmailGestor', PortLotacaoGestor='$LotacaoGestor', PortFinalidade='$Finalidade', PortAbrangencia='$Abrangencia', PortCadeiaDeValor='$CadeiaDeValor', PortHierarquia='$Hierarquia', PortDetalhamento='$Detalhamento', PortIndicadores='$Indicadores', PortMaturidade='$Maturidade', PortSituacao='Público', PortDataModificacao='$DataAtual', PortIPModificacao='$IPusuario' WHERE PortID = '$IDmodelo'");

if($atualizar){echo '<p align=center>Editado com sucesso!</p>';}else{echo '<p align=center>Não editado!</p><br>'; $erro=$conecta->errorInfo(); print_r($erro);}

////////  MANTÉM O NOME ATUAL DO ARQUIVO E DO DIAGRAMA  ///////////////////////////////////////
$NomeAtualArquivo = explode('/', $NomeArquivo);
$NomeNovoArquivo = $NomeAtualArquivo[1];
$NomeArquivo = $NomeNovoArquivo;

$IDDiagrama = explode('.', $NomeArquivo);
   if($Hierarquia == 'Macroprocesso') 
   { 
   $NomeAtualDiagrama = $IDDiagrama[0].".svg";
   }
   else
   {
   $NomeAtualDiagrama = $IDDiagrama[0].".png";       
   }
$NomeNovoDiagrama = $NomeAtualDiagrama;
$NomeDiagrama = $NomeNovoDiagrama;

////////  INICIA O UPLOAD DO ARQUIVO  ///////////////////////////////////////
$pasta = 'arquivos/';
$tamanhoarquivo = $_FILES['arquivo']['size'];
$tipoarquivo	= $_FILES['arquivo']['type'];

//////  PARA FILTRAR TAMANHO DO ARQUIVO "só passa: até 20MB": ////////////////
if($tamanhoarquivo > 30000000) 
  { 
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>O tamanho do arquivo é maior que o limite permitido!</p>";
exit; 
  } 

////////   COPIA ARQUIVO NO DESTINO   /////////////////////////////////
$uploadfile = $pasta.$NomeArquivo;

if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $uploadfile))
 {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Upload Realizado com Sucesso!</p><p align='center'><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>";
 }
else 
  {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
//echo "<p align='center'>Upload Não Realizado! ".mysql_error()."</p>";
  }

////////   COPIA DIAGRAMA NO DESTINO   /////////////////////////////////
if ($NovoDiagrama)
 {
unlink($pasta.$NomeDiagrama); //tive que forçar o apagamento do arquivo de imagem anterior porque o servidor não deixava sobrescrevê-lo
 }

$uploadfile = $pasta.$NomeDiagrama;

if (move_uploaded_file($_FILES['diagrama']['tmp_name'], $uploadfile))
 {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Upload Realizado com Sucesso!</p>";
 }
else 
  {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
//echo "<p align='center'>Upload Não Realizado! ".mysql_error()."</p>";  NÃOFUNCIONA NO PHP 7 - PDO
  }

//Finaliza processo
echo ("<p align='center'>Ação efetivada!<br><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>");
?>
<?
### RECURSIVO ##
  }            #           
################
?>
