<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<?
### RECURSIVO #####
 if (!$_POST)   { #           
###################
?>
<font face=arial>
<script>
function inseririd(campo)
 {
  if (campo=='sim')
   {
document.getElementById('informaid').innerHTML = "<input type=text size=40 name=IDVersaoPublicada pattern='[0-9]+$' placeholder='Informe o ID da versão pública atual do processo' required>";
   }
  else
   {
document.getElementById('informaid').innerHTML = "";
   }
 }
</script>

<title>Submeter Processo</title>
<h4 align=center>Submeter processo para validação e publicação no Portfólio de Processos AGU</h4>
<h5 align=center>Antes de submeter um modelo certifique-se de que esse esteja alinhado<br>à <a href='documentos/PoliticaGovernancaProcessosTrabalho.pdf' target=blank>Política de Governança de Processos</a> da AGU e que, também, atenda a todas<br>as recomendações da <a href='documentos/SistematicaProcessosTrabalhoAGU.pdf' target=blank>Sistemática para Modelagem de Processos de Trabalho!</a></h5>
<form method=post enctype="multipart/form-data" action="<? $PHP_SELF ?>"> 
<table align=center>
<tr><td><input type=text size=40 name=NomeProcesso placeholder="Informe o Nome do Processo" required></td></tr>
<tr><td><input type=text size=40 name=CPFGestor placeholder="Informe o CPF do Gestor do Processo" required></td></tr>
<tr><td><input type=email size=40 name=EmailGestor placeholder="Informe o Email do Gestor do Processo" required></td></tr>
<tr><td><input type=text size=40 name=LotacaoGestor placeholder="Informe a Lotação do Gestor do Processo" required></td></tr>
<tr><td><select name=Finalidade required>
<option value=''>Selecione a finalidade do processo</option>
<option>Finalístico</option>
<option>Apoio</option>
<option>Gerencial</option>
</select></td></tr>
<tr><td><select name=Abrangencia required>
<option value=''>Selecione a abrangência do processo</option>
<option>Corporativo</option>
<option>Setorial</option>
</select></td></tr>
<tr><td><select name=CadeiaDeValor required>
<option value=''>Selecione a posição do processo na Cadeia de Valor</option>
<option>Definir Diretrizes e Estratégia</option>
<option>Manter Relacionamentos</option>
<option>Entregar Produtos e Serviços</option>
<option>Suportar Processos</option>
</select></td></tr>
<tr><td><select name=Hierarquia required>
<option value=''>Selecione a posição hierarquica do processo</option>
<option>Macroprocesso</option>
<option>Processo</option>
<option>Subprocesso</option>
</select></td></tr>
<tr><td><select name=Detalhamento required>
<option value=''>Selecione o nível de detalhamento do processo</option>
<option>Visão Geral</option>
<option>Negócio</option>
<option>Sistema</option>
</select></td></tr>
<tr><td><input type=radio name=Versao value=primeira onclick="inseririd('nao')" required>Primeira versão<input type=radio name=Versao value=nova onclick="inseririd('sim')">Nova versão</td></tr>
<tr><td id=informaid></td></tr>
</table>
<br>
<table align=center>
<tr><td colspan=2 align=center><b>Anexe um arquivo compactado contendo o arquivo Bizagi do MODELO<br> do processo, todos os seus ARTEFATOS e a lista de <a href='documentos/ListaDeIndicadores.docx' target=blank>INDICADORES</a>:<br><br></b></td></tr>
<tr>
<td><input type="file" name="arquivo" required></td>
<td><input type="submit" name="enviar" value="Enviar arquivo"></td>
<input type="hidden" name=DataAtual value="<?=date('Y-m-d H:i:s')?>">
<input type="hidden" name=IPusuario value="<?=$_SERVER['REMOTE_ADDR']?>">
<input type="hidden" name="MAX_FILE_SIZE" value="1024">
</tr></table>
</form> 
<?
### RECURSIVO ###
  } else {      #           
#################

////////   CADASTRA PROCESSO NO BANCO   /////////////////////////////////
$NomeProcesso  = $_POST['NomeProcesso'];
$CPFGestor     = $_POST['CPFGestor'];
$EmailGestor   = $_POST['EmailGestor'];
$LotacaoGestor = $_POST['LotacaoGestor'];
$Finalidade    = $_POST['Finalidade'];
$Abrangencia   = $_POST['Abrangencia'];
$CadeiaDeValor = $_POST['CadeiaDeValor'];
$Hierarquia    = $_POST['Hierarquia'];
$Detalhamento  = $_POST['Detalhamento'];
$Versao        = $_POST['Versao'];
$DataAtual     = $_POST['DataAtual'];
$IPusuario     = $_POST['IPusuario'];
$NomeArquivo   = $_FILES['arquivo']['name'];

////////  RENOMEIA O ARQUIVO PARA ELIMINAR ACENTOS, ESPAÇOS E MAIÚSCULAS ////////
function renomear($string) 
 {
$string = preg_replace("/[ÁÀÂÃÄáàâãä]/", "a", $string);
$string = preg_replace("/[ÉÈÊéèê]/", "e", $string);
$string = preg_replace("/[ÍÌíì]/", "i", $string);
$string = preg_replace("/[ÓÒÔÕÖóòôõö]/", "o", $string);
$string = preg_replace("/[ÚÙÜúùü]/", "u", $string);
$string = preg_replace("/[Çç]/", "c", $string);
$string = preg_replace("/[][><}{)(:;,!?*%~^`@]/", "", $string);
$string = preg_replace("/ /", "", $string);
$string = strtolower($string);
return $string;
 }
$NomeArquivo = renomear($NomeArquivo);

require('conectabd.php');

//Se for a Primeira Versão verifica se o nome do processo já existe
if ($Versao == 'primeira')
 {
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortNomeProcesso='$NomeProcesso'");

  while($dados = $select->fetch(PDO::FETCH_OBJ))
   {
  $JaNome = $dados->PortNomeProcesso;
  $JaID = $dados->PortID;
   }

  if (isset($JaNome) == '')
   {   
  $VersaoModelo = 1;
   }
  else
   {
   echo "<p align=center>Esse nome de processo já existe para o ID ".$JaID."</p><p align='center'><a href='javascript:window.history.go(-1)'>Voltar</a></p>";
   exit;
   }
 }

//Se for uma nova versão, consulta a versão antiga pelo ID informado para somar a versão
if ($Versao == 'nova')
 {
$IDModeloPublicado = $_POST['IDVersaoPublicada'];
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDModeloPublicado'");

  while($dados = $select->fetch(PDO::FETCH_OBJ))
   {
   $VPublicada = $dados->PortVersao;
   }
    if (isset($VPublicada))
     {
     $VersaoModelo = $VPublicada+1;
     }
    else
     {
     echo "<p align=center>O ID informado não existe!</p><p align='center'><a href='javascript:window.history.go(-1)'>Voltar</a></p>";
     exit;    
     }
 }
$NomeArquivoCaminho = "arquivos/".$NomeArquivo;

//grava os metadados do modelo no banco de dados
if (!isset($IDModeloPublicado)){$IDModeloPublicado = NULL;}

$inserir = $conecta->exec("INSERT INTO TabPortfolio (PortNomeProcesso, PortCPFgestor, PortEmailGestor, PortLotacaoGestor, PortNomeArquivo, PortFinalidade, PortAbrangencia, PortCadeiaDeValor, PortHierarquia, PortDetalhamento, PortVersao, PortSituacao, PortIDVersaoAnterior, PortDataModificacao, PortIPModificacao)
             VALUES ('$NomeProcesso', '$CPFGestor', '$EmailGestor', '$LotacaoGestor', '$NomeArquivoCaminho', '$Finalidade', '$Abrangencia', '$CadeiaDeValor', '$Hierarquia', '$Detalhamento', '$VersaoModelo', 'Validar', '$IDModeloPublicado', '$DataAtual', '$IPusuario')");
             if($inserir){echo '<p align=center>Inserido com sucesso!</p>';}else{echo '<p align=center>Não inserido!</p><br>'; $erro=$conecta->errorInfo(); print_r($erro);}

//Renomeia o arquivo com o ID gerado no cadastro
$Arquivo = explode('.', $NomeArquivo);
$IDgerado = $conecta->lastInsertId();
$NomeArquivoID = "arquivos/".$IDgerado.".".$Arquivo[1];
$atualizar = $conecta->exec("UPDATE TabPortfolio SET PortNomeArquivo = '$NomeArquivoID' WHERE PortID = '$IDgerado'");

$NomeArquivo = $IDgerado.".".$Arquivo[1];

////////  INICIA O UPLOAD DO ARQUIVO  ///////////////////////////////////////
$pasta = 'arquivos/';
$tamanhoarquivo = $_FILES['arquivo']['size'];
$tipoarquivo	= $_FILES['arquivo']['type'];

//////  PARA FILTRAR TAMANHO DO ARQUIVO "só passa: até 20MB": ////////////////
if($tamanhoarquivo > 30000000) 
  { 
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>O tamanho do arquivo é maior que o limite permitido!</p>";
exit; 
  } 

////////   COPIA ARQUIVO NO DESTINO   /////////////////////////////////
$uploadfile = $pasta.$NomeArquivo;

if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $uploadfile))
 {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Upload Realizado com Sucesso!</p><p align='center'><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>";
 }
else 
  {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Upload Não Realizado! ".mysql_error()."</p>";
  }
?>
<?
### RECURSIVO ##
  }            #           
################
?>
