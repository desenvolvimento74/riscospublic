<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<?
## RECURSIVO ####
  if (!$_POST){ #
#################
?>
<?
$PropostaNum = $_GET['PropostaNum'];
$DataAtual = date('Y-m-d H:i:s');

require('conectabd.php');

echo "<font face=arial><table align=center width=50%>";
$select = $conecta->query("SELECT * FROM TabMelhoria WHERE MeNumero='$PropostaNum'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
//verifique se já existe reposta gravada
if (!isset($dados->MeResposta)){$dados->MeResposta=NULL; $Botao="<input type=submit value=Enviar onclick=confirm('Confima?')>";}else{$Fechar="<p align=center><a href='javascript:window.close()'>Fechar</a></p>";}
//apresenta o formulário com o resposta ou sem resposta para envio.
?>
<th>TRATAR PROPOSTA DE MELHORIA DE PROCESSO</th>
<tr><td></td></tr>
<tr><td align=center>Proposta número: <i><b><?=$dados->MeNumero?></i></b> criada em: <i><b><?=date('d/m/Y H:m:i', strtotime($dados->MeDataCriacao))?></i></b> autor: <i><b><?=$dados->MeEmailPropositor?></i></b></tr>
<tr><td align=center>Processo de trabalho: <i><b>(id:<?=$dados->MeIDModelo?>) <?=$dados->MeModelo?></i></b></td></tr>
<tr><td align=center>Gestor: <i><b><?=$dados->MeGestorModelo?></i></b></td></tr>
<tr><td><b>Texto da proposta:</b></td></tr>
<tr><td><i><?=$dados->MeProposta?></i></td></tr>
<tr><td></td></tr>
<tr><td><b>Resposta:</b></td></tr>
<form method=post action="<? $PHP_SELF ?>">
<tr><td align=center><textarea rows=10 cols=80 name=Resposta maxlength=450 placeholder="Descreva aqui a resposta para a proposta de melhoria do processo..." required><?=$dados->MeResposta?></textarea></td></tr>
<tr><td align=center><?=$Botao?></td></tr>
<input type=hidden name=MeNumero value="<?=$dados->MeNumero?>">
<input type=hidden name=MeIDModelo value="<?=$dados->MeIDModelo?>">
<input type=hidden name=MeModelo value="<?=$dados->MeModelo?>">
<input type=hidden name=MeGestorModelo value="<?=$dados->MeGestorModelo?>">
<input type=hidden name=MeProposta value="<?=$dados->MeProposta?>">
<input type=hidden name=MeEmailPropositor value="<?=$dados->MeEmailPropositor?>">
<input type=hidden name=MeDataCriacao value="<?=$dados->MeDataCriacao?>">
</form>
<?
 }
echo "</table></font>";
echo $Fechar;

## RECURSIVO ###
   } else {    #
################

$MeNumero = $_POST['MeNumero'];
$Resposta = $_POST['Resposta'];
$MeIDModelo = $_POST['MeIDModelo'];
$MeModelo = $_POST['MeModelo'];
$MeGestorModelo = $_POST['MeGestorModelo'];
$MeProposta = $_POST['MeProposta'];
$MeEmailPropositor = $_POST['MeEmailPropositor'];
$MeDataCriacao = $_POST['MeDataCriacao'];
$DataAtual = date('Y-m-d H:i:s');
//strtotime($dados->MeDataCriacao);

//grava a resposta no banco de dados
require('../sistema/conectabd.php');
$atualizar = $conecta->exec("UPDATE TabMelhoria SET MeResposta='$Resposta', MeEstado='Tratada', MeDataResposta='$DataAtual' WHERE MeNumero = '$MeNumero'");
if($atualizar){echo 'Editado com sucesso!';}else{echo 'Não editado!<br>'; $erro=$conecta->errorInfo(); print_r($erro);}

//Envia ao Propositor, por email, a comunicação da resposta (com cópia para o EGOP)
$Para = $MeEmailPropositor;
$Assunto = "Resposta de proposta de melhoria de processo de trabalho";
$Outros  ="From: Portfólio EGOP <consulte@pierconsultoria.com.br>". "\r\n"; 
$Outros .="Bcc: adinilson.silva@presidencia.gov.br". "\r\n";
$Outros .= "MIME-Version: 1.0". "\r\n"; 
$Outros .= "Content-type: text/html; charset=iso-8859-1". "\r\n"; 
$Mensagem = "<html>
<h5>Olá!</h5>
<p>Em ".date('d/m/Y H:m:i')." você enviou uma proposta de melhoria para o processo de trabalho <b>(id:".$MeIDModelo.") ".$MeModelo."</b> com a seguinte descrição:</p>
<p><i>".$MeProposta."</i></p>
<p>Sua proposta foi avaliada pelo Gestor do Processo que registrou o seguinte parecer:</p>
<p><i>".$Resposta."</i></p>
<p>Esperamos que a resposta tenha sido satisfatória!</p>
<p>Você pode submeter outras propostas para esse ou outros processos de trabalho publicados no <a href='http://www.pierconsultoria.com.br/agudge/portfolio/modelo.php?IDmodelo=".$IDModelo."'>Portólio de Processos de Trabalho da AGU</a></p>
<p>Atenciosamente,<br>Escritório de Governança de Processos de Trabalho AGU (EGOP)</p>
</html>";

mail($Para, mb_encode_mimeheader($Assunto), $Mensagem, $Outros);

echo "<br><br><p align='center'>Resposta registrada com sucesso!<br>As partes interessadas serão comunicadas sobre o seu parecer.</p><p align='center'><a href='javascript:window.close()'>Fechar</a></p>";

## RECURSIVO ###
         }     #
################
?>
