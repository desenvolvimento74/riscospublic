<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<?
### RECURSIVO #####
 if (!$_POST)   { #           
###################
?>
<?
require('conectabd.php');

$IDmodelo =  $_GET['IDmodelo'];
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDmodelo'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
$Arquivo = explode('.', $dados->PortNomeArquivo);
if (isset($dados->PortIDVersaoAnterior)){$IDmodeloAnterior = $dados->PortIDVersaoAnterior;}else{$IDmodeloAnterior = NULL;}
?>
<font face=arial>
<title>Publicar Modelo</title>
<h4 align=center>Publicar Modelo de Processo no Portfólio de Processos AGU</h4>
<h5 align=center>Antes de publicar um modelo certifique-se de que esse esteja alinhado<br>à <a href='documentos/PoliticaGovernancaProcessosTrabalho.pdf' target=blank>Política de Governança de Processos</a> da PR e que, também, atenda a todas<br>as recomendações da <a href='documentos/SistematicaProcessosTrabalhoAGU.pdf' target=blank>Metodologia de Governança de Processos de Trabalho!</a></h5>
<form method=post enctype="multipart/form-data" action="<? $PHP_SELF ?>"> 
<table align=center>
<tr><td>ID:</td><td><input type=text size=40 name=IDmodelo value="<?=$dados->PortID?>" title='ID do modelo' readonly></td></tr>
<tr><td>Versão:</td><td><input type=text size=40 name=Versao value="<?=$dados->PortVersao?>" title='Versão do modelo' readonly></td></tr>
<tr><td><i>ID Versão anterior</i>:</td><td><input type=text size=40 name=IDmodeloAnterior value="<?=$IDmodeloAnterior?>" title="ID do modelo da versão anterior" readonly></td></tr>
<tr><td>Nome Processo:</td><td><input type=text size=40 name=NomeProcesso value="<?=$dados->PortNomeProcesso?>" required readonly></td></tr>
<tr><td>CPF Gestor:</td><td><input type=text size=40 name=CPFGestor value="<?=$dados->PortCPFgestor?>" required></td></tr>
<tr><td>Email Gestor:</td><td><input type=email size=40 name=EmailGestor value="<?=$dados->PortEmailGestor?>" required></td></tr>
<tr><td>Lotação Gestor:</td><td><input type=text size=40 name=LotacaoGestor value="<?=$dados->PortLotacaoGestor?>" required></td></tr>
<tr><td>Finalidade:</td><td><select name=Finalidade required>
<option value=''>Selecione a finalidade do processo</option>
<option selected><?=$dados->PortFinalidade?></option>
<option>Finalístico</option>
<option>Apoio</option>
<option>Gerencial</option>
</select></td></tr>
<tr><td>Abrangência:</td><td><select name=Abrangencia required>
<option value=''>Selecione a abrangência do processo</option>
<option selected><?=$dados->PortAbrangencia?></option>
<option>Corporativo</option>
<option>Setorial</option>
</select></td></tr>
<tr><td>Cadeia de Valor:</td><td><select name=CadeiaDeValor required>
<option value=''>Selecione a posição do processo na Cadeia de Valor</option>
<option selected><?=$dados->PortCadeiaDeValor?></option>
<option>Definir Diretrizes e Estratégia</option>
<option>Manter Relacionamentos</option>
<option>Entregar Produtos e Serviços</option>
<option>Suportar Processos</option>
</select></td></tr>
<tr><td>Hierarquia:</td><td><select name=Hierarquia required>
<option value=''>Selecione a posição hierarquica do processo</option>
<option selected><?=$dados->PortHierarquia?></option>
<option>Macroprocesso</option>
<option>Processo pai</option>
<option>Subprocesso</option>
</select></td></tr>
<tr><td>Detalhamento:</td><td><select name=Detalhamento required>
<option value=''>Selecione o nível de detalhamento do processo</option>
<option selected><?=$dados->PortDetalhamento?></option>
<option>Visão Geral</option>
<option>Negócio</option>
<option>Sistema</option>
</select></td></tr>
<tr><td>Maturidade:</td><td><select name=Maturidade required>
<option value=''>Selecione a maturidade do processo</option>
<option selected><?=$dados->PortMaturidade?></option>
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
<option>6</option>
</select></td></tr>
</table>
<br>
<table align=center>
<tr><td colspan=2 align=center><b>Anexe o diagrama do processo:</b></td></tr>
<tr>
<td><input type="file" name="arquivo" required></td>
<td><input type="submit" name="enviar" value="Enviar arquivo"></td>
<input type="hidden" name=DataAtual value="<?=date('Y-m-d H:i:s')?>">
<input type="hidden" name=IPusuario value="<?=$_SERVER['REMOTE_ADDR']?>">
<input type="hidden" name="MAX_FILE_SIZE" value="1024">
</tr></table>
</form> 
<?
 }
### RECURSIVO ###
  } else {      #           
#################

$IDmodelo      = $_POST['IDmodelo'];
$Versao        = $_POST['Versao'];
if (isset($_POST['IDmodeloAnterior'])){$IDmodeloAnterior = $_POST['IDmodeloAnterior'];}else{$IDmodeloAnterior = NULL;}
$NomeProcesso  = $_POST['NomeProcesso'];
$CPFGestor     = $_POST['CPFGestor'];
$EmailGestor   = $_POST['EmailGestor'];
$LotacaoGestor = $_POST['LotacaoGestor'];
$Finalidade    = $_POST['Finalidade'];
$Abrangencia   = $_POST['Abrangencia'];
$CadeiaDeValor = $_POST['CadeiaDeValor'];
$Hierarquia    = $_POST['Hierarquia'];
$Detalhamento  = $_POST['Detalhamento'];
$Maturidade    = $_POST['Maturidade'];
$DataAtual     = $_POST['DataAtual'];
$DataAtual     = $_POST['DataAtual'];
$IPusuario     = $_POST['IPusuario'];
//$NomeArquivo   = $_FILES['arquivo']['name'];
if ($Hierarquia == 'Macroprocesso')
 {
 $NomeArquivo = $_POST['IDmodelo'].".svg";
 }
 else
 {
 $NomeArquivo = $_POST['IDmodelo'].".png";
 }

$NomeArquivoGravar = "arquivos/".$NomeArquivo;

require('conectabd.php');

//Se for a Primeira Versão verifica se o nome do processo já existe
if ($Versao == 1)
 {
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortNomeProcesso='$NomeProcesso' AND PortSituacao='Público'");

  while($dados = $select->fetch(PDO::FETCH_OBJ))
   {
  $JaNome = $dados->PortNomeProcesso;
  $JaID = $dados->PortID;
   }

  if (isset($JaNome) == '')
   {   
  $VersaoModelo = 1;
   }
  else
   {
   echo "<p align=center>Esse nome de processo já existe para o ID ".$JaID."</p><p align='center'><a href='javascript:window.history.go(-1)'>Voltar</a></p>";
   exit;
   }
 }

//Atualiza metadados do modelo de processo
$atualizar = $conecta->exec("UPDATE TabPortfolio SET PortCPFgestor='$CPFGestor', PortEmailGestor='$EmailGestor', PortLotacaoGestor='$LotacaoGestor', PortFinalidade='$Finalidade', PortAbrangencia='$Abrangencia', PortCadeiaDeValor='$CadeiaDeValor', PortHierarquia='$Hierarquia', PortDetalhamento='$Detalhamento', PortMaturidade='$Maturidade', PortSituacao='Público', PortDataModificacao='$DataAtual', PortIPModificacao='$IPusuario' WHERE PortID = '$IDmodelo'");

if($atualizar){echo '<p align=center>Editado com sucesso!</p>';}else{echo '<p align=center>Não editado!</p><br>'; $erro=$conecta->errorInfo(); print_r($erro);}

//se for nova versão executa o arquivamento e informa do arquivamento da versão anterior
if ($Versao > 1)
 {
    $select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDmodeloAnterior'");
    
    while($dados = $select->fetch(PDO::FETCH_OBJ))
     {
     $VersaoAnterior = $dados->PortVersao;
     $ArquivoNomeAntigo = $dados->PortNomeArquivo;
     $Arquivo = explode('.', $dados->PortNomeArquivo);
     }
$ArquivoNomeVersao = $Arquivo[0]."_".$VersaoAnterior.".".$Arquivo[1];
        if ($Hierarquia == 'Macroprocesso')
         {
        $DiagramaNomeAntigo = $Arquivo[0].".svg";
        $ArquivoNomeVersaoDiagrama = $Arquivo[0]."_".$VersaoAnterior.".svg";
         }
        else
         {
        $DiagramaNomeAntigo = $Arquivo[0].".png";
        $ArquivoNomeVersaoDiagrama = $Arquivo[0]."_".$VersaoAnterior.".png";             
         }

$atualizar = $conecta->exec("UPDATE TabPortfolio SET PortSituacao='Arquivado', PortNomeArquivo='$ArquivoNomeVersao' WHERE PortID = '$IDmodeloAnterior'");

rename($ArquivoNomeAntigo, $ArquivoNomeVersao);
rename($DiagramaNomeAntigo, $ArquivoNomeVersaoDiagrama);

echo "<p align='center'>A versão anterior do modelo foi arquivada!</p>";
 }

////////  INICIA O UPLOAD DO DIAGRAMA DO MODELO  ///////////////////////////////////////
//$arquivo=$_POST['arquivo'];
$pasta = 'arquivos/';
$tamanhoarquivo = $_FILES['arquivo']['size'];
$tipoarquivo	= $_FILES['arquivo']['type'];

//////  PARA FILTRAR TAMANHO DO ARQUIVO "só passa: até 20MB": ////////////////
if($tamanhoarquivo > 30000000) 
  { 
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>O tamanho do arquivo é maior que o limite permitido!</p>";
exit; 
  } 

//////  PARA FILTRAR EXTENSÃO DO ARQUIVO "só passa: png": //////////////////
$separanome = explode(".",$NomeArquivo);
$extensao = $separanome[1];

if (($extensao != "png") and ($extensao != "svg"))
  {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Extensão de arquivo não permitida!</p>";
exit; 
  }

////////   COPIA ARQUIVO NO DESTINO   /////////////////////////////////
$uploadfile = $pasta.$NomeArquivo;

if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $uploadfile))
 {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
echo "<p align='center'>Upload Realizado e Processo Publicado com Sucesso!</p>";
echo "<p align='center'><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>";
echo "<body onunload='window.opener.location.reload()'>";
 }
else 
  {
echo"<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
//echo "<p align='center'>Upload Não Realizado! ".mysql_error()."</p>"; NÃO FUNCIONA NO PHP 7 - PDO
  }
?>
<?
### RECURSIVO ##
  }            #           
################
?>
