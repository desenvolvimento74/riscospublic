<?
##############################
# adinilson.silva@agu.gov.br #
##############################
?>
<style>
body {background-color: lightgray;}
TH {font-family:arial; padding-left:1cm;}

a:link  {text-decoration:none; color:#056BB6;}
a:hover {color:red;}
</style>

<title>Portfólio de Processos</title>
<table width=100% style="background-color:white;border-radius: 10px 10px 10px 10px;"><tr>
<td width='15%' rowspan=3><img src='../brasaooficial.png' width='75%'></td>
<td align=center><a style="text-shadow: 2px 2px 2px #A9BCF5; color:#056BB6; font-family:arial; font-size:34">Cadastro de Riscos</a></td></tr>

<?
require('conectabd.php');

$IDmodelo =  $_GET['IDmodelo'];

$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDmodelo'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
if(isset($dados->PortIDVersaoAnterior)){$VersaoAnterior = $dados->PortIDVersaoAnterior;}
  if($VersaoAnterior > 0){$VersaoAnterior = "<a href='modelo.php?IDmodelo=".$dados->PortIDVersaoAnterior."' style='color:white; font-size:14;text-decoration:none;'>(anterior)</a>";}else{$VersaoAnterior = '';}

$Riscos = "<a href='riscos.php/?IDmodelo=".$IDmodelo."' style='font-family:arial; font-size:12;' target='_blank'><b>Riscos</b></a>";

$Situacao = $dados->PortSituacao;
$Arquivo = explode('.', $dados->PortNomeArquivo);

  if ($Situacao != 'Arquivado')
   {
  $LocalizacaoDocumentacao = $dados->PortNomeArquivo;
  $LocalizacaoDiagrama = $Arquivo[0].".png";
   }
  else
   {
  $LocalizacaoDocumentacao = $Arquivo[0].$dados->PortIDVersaoAnterior.".".$Arquivo[1];
  $LocalizacaoDiagrama = $Arquivo[0].$dados->PortIDVersaoAnterior.".png";
   }
$NomeProcesso=$dados->PortNomeProcesso;
//prepara inclusão do link de proposta de elhoria se o modelo é a versão vigente
  if ($Situacao=='Público'){$Proposta = " &nbsp;&nbsp;&nbsp;&nbsp;<a href='proposta.php?ID=".$dados->PortID."&Modelo=".$dados->PortNomeProcesso."&Gestor=".$dados->PortEmailGestor."' target='_blank'><b>Propor MELHORIA</b>"; }else{$Proposta ='';}

echo "<tr><td align=center style='color:#056BB6; font-size:30;'>".$dados->PortNomeProcesso." <a style='color:#056BB6; font-size:14;'>(versão: ".$dados->PortVersao.")</a> ".$VersaoAnterior." ".$Riscos."</td></tr>";
echo "<tr><td align=center style='color:#056BB6;font-family:arial; font-size:12;'><b>ID</b>: ".$dados->PortID." &nbsp;&nbsp;&nbsp;&nbsp;<b>Finalidade</b>: ".$dados->PortFinalidade." &nbsp;&nbsp;&nbsp;&nbsp;<b>Abrangência</b>: ".$dados->PortAbrangencia." &nbsp;&nbsp;&nbsp;&nbsp;<b>Cadeia de Valor</b>: ".$dados->PortCadeiaDeValor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Gestor</b>: ".$dados->PortEmailGestor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Lotação</b>: ".$dados->PortLotacaoGestor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Download:</b> <a href='".$LocalizacaoDocumentacao."'>Documentação</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href='../painel' target='_blank'><b>INDICADORES</b></a>".$Proposta."</td></tr>";
 }
?>
</table>

<style>
table.bordasimples {border-collapse: collapse;}
table.bordasimples th {border:1px solid #FF0000; font-family:arial; font-size:10; padding-left:0cm;}
table.bordasimples tr td {border:1px solid #FF0000;font-family:arial; font-size:10;}
</style>

<br><center>
<a href='../riscoscadastrar.php?IDmodelo=<?=$IDmodelo?>'>Cadastrar Risco</a>
</center><br>
<font face=arial><b>Lista de Riscos Identificados Para o Processo de Trabalho:</b></font>
<table class=bordasimples>
<th id=th></th><th id=th>Id</th><th id=th>Número da tarefa</th><th id=th>Texto da tarefa</th><th id=th><font color=blue>Descrição do Risco</th><th id=th>Polaridade</th><th id=th>Probabilidade</th><th id=th>Impacto</th><th id=th>Nível</th><th id=th>Tipologia</th><th id=th>Estratégia</th><th id=th>Situação</th><th id=th><font color=blue>Descrição do Controle</th><th id=th>Benefícios</th><th id=th>Função</th><th id=th>Custo da implantação</th><th id=th>Ponto da implantação</th><th id=th>Área Responsável</th><th id=th>Email do responsavel</th><th id=th>Data de início da implantação</th><th id=th> Data fim da implantação</th><th id=th>Situação</th><th id=th>Observações</th>
<?
$select = $conecta->query("SELECT * FROM TabRiscos WHERE RisIDmodelo='$IDmodelo'");
$i=1;
while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
$RisID = $dados->RisID;
$RisIDmodelo = $dados->RisIDmodelo;
$RisProcesso = $dados->RisProcesso;
$RisProcessoVersao = $dados->RisProcessoVersao;
$RisTarefaNum = $dados->RisTarefaNum;
$RisTarefa = $dados->RisTarefa;
$RisRisco = $dados->RisRisco;
$RisPolaridade = $dados->RisPolaridade;
$RisProbabilidade = $dados->RisProbabilidade;
$RisImpacto = $dados->RisImpacto;
$RisNivel = $dados->RisNivel;
$RisTipologia = $dados->RisTipologia;
$RisEstrategia = $dados->RisEstrategia;
$RisSituacao = $dados->RisSituacao;
$RisControle = $dados->RisControle;
$RisConBeneficios = $dados->RisConBeneficios;
$RisConFuncao = $dados->RisConFuncao;
$RisConCusto = $dados->RisConCusto;
$RisConLocal = $dados->RisConLocal;
$RisConArea = $dados->RisConArea;
$RisConResponsavel = $dados->RisConResponsavel;
$RisConDataInicio = $dados->RisConDataInicio;
$RisConDataFim = $dados->RisConDataFim;
$RisConSituacao = $dados->RisConSituacao;
$RisConObservacoes = $dados->RisConObservacoes;
$RisDataCadastro = $dados->RisDataCadastro;
$RisEmailCadastrou = $dados->RisEmailCadastrou;
$RisControlarComo = $dados->RisControlarComo;

$variaveisget = "<a href='../riscoscadastrar.php?RisID=".$RisID."&IDmodelo=".$RisIDmodelo."&RisIDmodelo=".$RisIDmodelo."&RisProcesso=".$RisProcesso."&RisProcessoVersao=".$RisProcessoVersao."&RisTarefaNum=".$RisTarefaNum."&RisTarefa=".$RisTarefa."&RisRisco=".$RisRisco."&RisPolaridade=".$RisPolaridade."&RisProbabilidade=".$RisProbabilidade."&RisImpacto=".$RisImpacto."&RisNivel=".$RisNivel."&RisTipologia=".$RisTipologia."&RisEstrategia=".$RisEstrategia."&RisSituacao=".$RisSituacao."&RisControle=".$RisControle."&RisConBeneficios=".$RisConBeneficios."&RisConFuncao=".$RisConFuncao."&RisConCusto=".$RisConCusto."&RisConLocal=".$RisConLocal."&RisConArea=".$RisConArea."&RisConResponsavel=".$RisConResponsavel."&RisConDataInicio=".$RisConDataInicio."&RisConDataFim=".$RisConDataFim."&RisConSituacao=".$RisConSituacao."&RisConObservacoes=".$RisConObservacoes."&RisDataCadastro=".$RisDataCadastro."&RisEmailCadastrou=".$RisEmailCadastrou."&RisControlarComo=".$RisControlarComo."'>".$RisID."</a>";
?>
<tr><td id=td><?=$i?></td><td id=td><?=$variaveisget?></td><td id=td><?=$dados->RisTarefaNum?></td><td id=td><?=$dados->RisTarefa?></td><td id=td><font color=blue><?=$dados->RisRisco?></td><td id=td><?=$dados->RisPolaridade?></td><td id=td><?=$dados->RisProbabilidade?></td><td id=td><?=$dados->RisImpacto?></td><td id=td><?=$dados->RisNivel?></td><td id=td><?=$dados->RisTipologia?></td><td id=td><?=$dados->RisEstrategia?></td><td id=td><?=$dados->RisSituacao?></td><td id=td><font color=blue><?=$dados->RisControle?></td><td id=td><?=$dados->RisConBeneficios?></td><td id=td><?=$dados->RisConFuncao?></td><td id=td><?=$dados->RisConCusto?></td><td id=td><?=$dados->RisConLocal?></td><td id=td><?=$dados->RisConArea?></td><td id=td><?=$dados->RisConResponsavel?></td><td id=td><?=$dados->RisConDataInicio?></td><td id=td><?=$dados->RisConDataFim?></td><td id=td><?=$dados->RisConSituacao?></td><td id=td><?=$dados->RisConObservacoes?></td></tr>
<?
$i++;
 }
?>
</table>


