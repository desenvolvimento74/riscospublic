<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<style>
TH {font-family:arial; padding-left:1cm;}
#l {text-align:center; padding-left:1cm;}
a:link  {text-decoration:none; color:white;}
a:hover {color:yellow;}
</style>


<title>Portfólio de Processos</title>
<table width=100% style="background-image: linear-gradient(to bottom right, yellow, green, blue); border-radius: 10px 10px 10px 10px;"><tr>
<td width='15%' rowspan=3><img src='brasaooficial.png' width='75%'></td>
<td align=center><a style="text-shadow: 2px 2px 2px lightgray; color:white; font-family:arial; font-size:34">Portfólio de Processos de Trabalho</a></td></tr>

<?
require('conectabd.php');

$IDmodelo =  $_GET['IDmodelo'];
$select = $conecta->query("SELECT * FROM TabPortfolio WHERE PortID='$IDmodelo'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
if(isset($dados->PortIDVersaoAnterior)){$VersaoAnterior = $dados->PortIDVersaoAnterior;}
  if($VersaoAnterior > 0){$VersaoAnterior = "<a href='modelo.php?IDmodelo=".$dados->PortIDVersaoAnterior."' style='color:white; font-size:14;text-decoration:none;'>(anterior)</a>";}else{$VersaoAnterior = '';}

$Riscos = "<a href='riscos.php/?IDmodelo=".$IDmodelo."' style='font-family:arial; font-size:12;' target='_blank'><b>Riscos</b></a>";

$Situacao = $dados->PortSituacao;
$Arquivo = explode('.', $dados->PortNomeArquivo);

  if ($Situacao != 'Arquivado')
   {
  $LocalizacaoDocumentacao = $dados->PortNomeArquivo;
      if ($dados->PortHierarquia == 'Macroprocesso')
       {
       $LocalizacaoDiagrama = $Arquivo[0].".svg";         
       }
      else
       {
       $LocalizacaoDiagrama = $Arquivo[0].".png";
       }
   }
  else
   {
  $LocalizacaoDocumentacao = $Arquivo[0].$dados->PortIDVersaoAnterior.".".$Arquivo[1];
      if ($dados->PortHierarquia == 'Macroprocesso')
       {
       $LocalizacaoDiagrama = $Arquivo[0].$dados->PortIDVersaoAnterior.".svg";        
       }
      else
       {
       $LocalizacaoDiagrama = $Arquivo[0].$dados->PortIDVersaoAnterior.".png";
       }
   }
$Hierarquia = $dados->PortHierarquia;
  if ($dados->PortHierarquia == 'Macroprocesso')
   {
   $Maturidade = " ";
   }
  else
   {
   $Maturidade = " <a style='color:white; font-size:14;'>maturidade: ".$dados->PortMaturidade."</a> ";
   }
//prepara inclusão do link de proposta de melhoria se o modelo é a versão vigente
  if ($Situacao=='Público'){$Proposta = " &nbsp;&nbsp;&nbsp;&nbsp;<a href='proposta.php?ID=".$dados->PortID."&Modelo=".$dados->PortNomeProcesso."&Gestor=".$dados->PortEmailGestor."' target='_blank'><b>Propor MELHORIA</b>"; }else{$Proposta ='';}

echo "<tr><td align=center style='color:white; font-size:30;'>".$dados->PortNomeProcesso." <a style='color:white; font-size:14;'>(versão: ".$dados->PortVersao.")</a> ".$VersaoAnterior.$Maturidade."<a href='riscos.php/?IDmodelo=".$IDmodelo."'  style='color:white;font-family:arial; font-size:12;' target='_blank'><b>Riscos</b></a></td></tr>";
echo "<tr><td align=center style='color:white;font-family:arial; font-size:12;'><b>ID</b>: ".$dados->PortID." &nbsp;&nbsp;&nbsp;&nbsp;<b>Finalidade</b>: ".$dados->PortFinalidade." &nbsp;&nbsp;&nbsp;&nbsp;<b>Abrangência</b>: ".$dados->PortAbrangencia." &nbsp;&nbsp;&nbsp;&nbsp;<b>Cadeia de Valor</b>: ".$dados->PortCadeiaDeValor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Gestor</b>: ".$dados->PortEmailGestor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Lotação</b>: ".$dados->PortLotacaoGestor." &nbsp;&nbsp;&nbsp;&nbsp;<b>Download:</b> <a href='".$LocalizacaoDocumentacao."'>Documentação</a> &nbsp;&nbsp;&nbsp;&nbsp;<a href='../painel' target='_blank'><b>INDICADORES</b></a>".$Proposta."</td></tr>";
 }

echo "</table>";
echo "<center>";
echo $dados->PortHierarquia;
if ($Hierarquia == 'Macroprocesso')
   {
   echo "<object type='image/svg+xml' data=".$LocalizacaoDiagrama."></object>";
   }
   else
   {
   echo "<img src=".$LocalizacaoDiagrama.">";
   }
echo "</center>";
?>
