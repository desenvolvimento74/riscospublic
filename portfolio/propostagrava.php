<?
######################################
# adinilson.silva@presidencia.gov.br #
######################################
?>
<?
$IDModelo = $_GET['ID'];
$Modelo = utf8_decode($_GET['Modelo']);
$GestorModelo = $_GET['Gestor'];
$EmailPropositor = $_GET['EmailPropositor'];
$Proposta = utf8_decode($_GET['Proposta']);
$DataAtual = date('Y-m-d H:i:s');

require('conectabd.php');

//Verifica se já foi gravada uma vez a confirmação de proposta, se não, grava.
$select = $conecta->query("SELECT * FROM TabMelhoria WHERE MeIDModelo='$IDModelo' AND MeProposta='$Proposta'");

while($dados = $select->fetch(PDO::FETCH_OBJ))
 {
$MeNumero = $dados->MeNumero;
 }

if (isset($MeNumero)) 
 {
echo "<p align=center>Esta proposta de melhoria já está registrada com o número ".$MeNumero."!</p><p align=center ><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>"; 
exit;
 }
 else
 {
$inserir = $conecta->exec("INSERT INTO TabMelhoria (MeEstado, MeIDModelo, MeModelo, MeGestorModelo, MeEmailPropositor, MeProposta, MeDataCriacao)
             VALUES ('Pendente', '$IDModelo', '$Modelo', '$GestorModelo', '$EmailPropositor', '$Proposta', '$DataAtual')");
            if($inserir){echo '<p align=center>Inserido com sucesso!</p>';}else{echo '<p align=center>Não inserido!</p><br>'; $erro=$conecta->errorInfo(); print_r($erro);}
echo "<p align=center>Proposta de melhoria registrada com sucesso!</p><p align=center>Aguarde o contato do Gestor do Processo de Trabalho!</p><p align=center ><a href='javascript:void()' onclick='window.close()'>Fechar</a></p>";

$MeNumeroGerado = mysql_insert_id();

//Envia ao Gestor do Processo, por email, a comunicação do registro da proposta (com cópia para o EGOP)
$Para = $GestorModelo;
$Assunto = "Avaliar proposta de melhoria de processo de trabalho";
$Outros  ="From: Portfólio EGOP <consulte@pierconsultoria.com.br>". "\r\n"; 
$Outros .="Bcc: adinilson.silva@agu.gov.br". "\r\n";
$Outros .= "MIME-Version: 1.0". "\r\n"; 
$Outros .= "Content-type: text/html; charset=iso-8859-1". "\r\n"; 
$Mensagem = "<html>
<h5>Olá!</h5>
<p>Uma parte interessada acaba de submeter uma proposta de melhoria para o processo <b>(id:".$IDModelo.") ".$Modelo."</b> que tem você designado como Gestor responsável no <a href='http://www.pierconsultoria.com.br/agudge/portfolio/modelo.php?IDmodelo=".$IDModelo."'>Portólio de Processos de Trabalho da AGU</a></p>
<p><b>Segue abaixo o texto da proposta de melhoria registrada:</b></p>
<p><i>".$Proposta."</i></p>
<p>Clique <a href='http://www.pierconsultoria.com.br/agudge/portfolio/propostaeditar.php?PropostaNum=".$MeNumeroGerado."'>aqui</a> para editar o tratamento da proposta de melhoria e,
 automaticamente, responder a parte interessada.</p>
<p>Atenciosamente,<br>Escritório de Governança de Processos de Trabalho AGU (EGOP)</p>
</html>";

mail($Para, mb_encode_mimeheader($Assunto), $Mensagem, $Outros); 
 }
?>
