-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 14/06/2022 às 14:23
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pierc250_presidencia`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `TabPortfolio`
--

CREATE TABLE `TabPortfolio` (
  `PortID` int(11) NOT NULL,
  `PortNomeProcesso` varchar(200) DEFAULT NULL,
  `PortCPFgestor` varchar(11) DEFAULT NULL,
  `PortEmailGestor` varchar(100) DEFAULT NULL,
  `PortLotacaoGestor` varchar(20) DEFAULT NULL,
  `PortNomeArquivo` varchar(150) DEFAULT NULL,
  `PortFinalidade` varchar(40) DEFAULT NULL,
  `PortAbrangencia` varchar(40) DEFAULT NULL,
  `PortCadeiaDeValor` varchar(40) DEFAULT NULL,
  `PortHierarquia` varchar(30) DEFAULT NULL,
  `PortDetalhamento` varchar(30) DEFAULT NULL,
  `PortVersao` varchar(5) DEFAULT NULL,
  `PortSituacao` varchar(30) DEFAULT NULL,
  `PortIDVersaoAnterior` varchar(10) DEFAULT NULL,
  `PortIndicadores` varchar(500) DEFAULT NULL,
  `PortMaturidade` varchar(2) DEFAULT NULL,
  `PortDataModificacao` datetime DEFAULT NULL,
  `PortIPModificacao` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `TabPortfolio`
--

INSERT INTO `TabPortfolio` (`PortID`, `PortNomeProcesso`, `PortCPFgestor`, `PortEmailGestor`, `PortLotacaoGestor`, `PortNomeArquivo`, `PortFinalidade`, `PortAbrangencia`, `PortCadeiaDeValor`, `PortHierarquia`, `PortDetalhamento`, `PortVersao`, `PortSituacao`, `PortIDVersaoAnterior`, `PortIndicadores`, `PortMaturidade`, `PortDataModificacao`, `PortIPModificacao`) VALUES
(1, 'CAV - Cadeia de Valor da PR', '3333333', 'jair.bolsonaro@presidencia.gov.br', 'PR', 'arquivos/1.zip', 'Finalístico', 'Corporativo', 'Definir Diretrizes e Estratégia', 'Macroprocesso', 'Visão Geral', '1', 'Público', '', 'www.google.com', '1', '2022-04-01 18:06:54', '170.246.252.101'),
(2, 'CONTAQ - Macroprocesso de Contratações e Aquisições', '3333333', 'thalita.oliveira@presidencia.gov.br', 'DILOG/SA/SG', 'arquivos/2.zip', 'Apoio', 'Corporativo', 'Suportar Processos', 'Macroprocesso', 'Visão Geral', '1', 'Público', '', 'www.google.com', '1', '2022-04-01 17:37:01', '170.246.252.101'),
(3, 'GERIS - Macroprocesso de Gestão de Riscos', '3333333', 'adinilson.silva@presidencia.gov.br', 'DGO/SE/SG', 'arquivos/3.zip', 'Apoio', 'Corporativo', 'Suportar Processos', 'Macroprocesso', 'Visão Geral', '1', 'Público', '', 'www.google.com', '1', '2022-04-01 18:03:50', '170.246.252.101'),
(4, 'GERIS - Processo de Gerenciamento de Riscos', '3333333', 'adinilson.silva@presidencia.gov.br', 'DGO/SE/SG', 'arquivos/4.zip', 'Apoio', 'Corporativo', 'Suportar Processos', 'Processo', 'Negócio', '1', 'Público', '', NULL, '2', '2022-04-01 17:59:12', '170.246.252.101');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `TabPortfolio`
--
ALTER TABLE `TabPortfolio`
  ADD PRIMARY KEY (`PortID`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `TabPortfolio`
--
ALTER TABLE `TabPortfolio`
  MODIFY `PortID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
