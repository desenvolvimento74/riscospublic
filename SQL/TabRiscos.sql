-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 15/06/2022 às 13:13
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `pierc250_presidencia`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `TabRiscos`
--

CREATE TABLE `TabRiscos` (
  `RisID` int(11) NOT NULL,
  `RisIDmodelo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisProcesso` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisProcessoVersao` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisTarefaNum` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisTarefa` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisRisco` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisPolaridade` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisProbabilidade` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisImpacto` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisNivel` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisTipologia` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisEstrategia` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisSituacao` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisControle` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConFuncao` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConBeneficios` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConCusto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConLocal` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConArea` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConResponsavel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConDataInicio` datetime DEFAULT NULL,
  `RisConDataFim` datetime DEFAULT NULL,
  `RisConSituacao` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisConObservacoes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RisDataCadastro` datetime DEFAULT NULL,
  `RisEmailCadastrou` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `TabRiscos`
--

INSERT INTO `TabRiscos` (`RisID`, `RisIDmodelo`, `RisProcesso`, `RisProcessoVersao`, `RisTarefaNum`, `RisTarefa`, `RisRisco`, `RisPolaridade`, `RisProbabilidade`, `RisImpacto`, `RisNivel`, `RisTipologia`, `RisEstrategia`, `RisSituacao`, `RisControle`, `RisConFuncao`, `RisConBeneficios`, `RisConCusto`, `RisConLocal`, `RisConArea`, `RisConResponsavel`, `RisConDataInicio`, `RisConDataFim`, `RisConSituacao`, `RisConObservacoes`, `RisDataCadastro`, `RisEmailCadastrou`) VALUES
(1, '4', 'GERIS - Processo de Gerenciamento de Riscos', '1', 'teste 1', 'teste 1', 'teste 1', 'Negativo', '4', '3', '12', 'Fraude', 'Tolerar', 'Em tratamento', 'teste 1', 'Corretivo', 'teste 1', '10', 'teste 1', 'teste 1', 'nil@dgo', '2022-04-13 00:00:00', '2022-04-29 00:00:00', 'Em implantação', 'teste 1', '2022-04-12 13:48:08', 'dgo@presidencia.gov.br');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `TabRiscos`
--
ALTER TABLE `TabRiscos`
  ADD PRIMARY KEY (`RisID`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `TabRiscos`
--
ALTER TABLE `TabRiscos`
  MODIFY `RisID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
