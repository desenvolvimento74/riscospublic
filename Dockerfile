
#FROM php:7.2.34-apache
FROM php:7.2-apache

RUN apt-get update && apt-get install -y \
    && docker-php-ext-install pdo pdo_mysql

COPY ./portfolio /var/www/html/

RUN chmod 777 /var/www/html/arquivos/

EXPOSE 80
